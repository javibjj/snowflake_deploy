
variable "databases" {
  type = map(object({
    name = string       #Name of the schema
    db_data_retention_in_days = number
    comment = string    #Comment for the schema
  }))
}
