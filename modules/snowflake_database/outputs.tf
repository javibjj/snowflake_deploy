output "database_name" {
  value = {
    for databases in snowflake_database.database :
    databases.name => databases.name
  }
}
