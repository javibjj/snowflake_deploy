resource "snowflake_database" "database" {
  for_each                    = "${var.databases}"
  name                        = each.value.name
  data_retention_time_in_days = each.value.db_data_retention_in_days
  comment                     = each.value.comment
}


/*Example:

module "snflk_db" {
    source = "../modules/snowflake_database"
    
    databases = {
        "DB1" = {
            name = "Main_DB"
            comment = "It is the main database"
            db_data_retention_in_days = 1
        },

        "DB2" = {
            name = "Temperory_DB"
            comment = "It is the temp database"
            db_data_retention_in_days = 1
        }     

    }
}*/